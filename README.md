# **Impulse Physics Simulator** #

## **About** ##
A simple 2D physics engine that handles collisions between objects using impulse resolution. The project began from an effort to build a multiplayer paddle game that dynamically changed depending on the number of players. The end goal is to build a robust framework that will aid in the creation of 2D games. The engine is based off the series of articles [How to Create a Custom 2D Physics Engine](http://gamedevelopment.tutsplus.com/tutorials/how-to-create-a-custom-2d-physics-engine-the-basics-and-impulse-resolution--gamedev-6331)  by Randy Gaul.

## **How to Use** ##
The engine assigns a set of physical properties to an object:

* Position (as x and y)
* Velocity (as x and y)
* Restitution (a number between 0 and 1)
* Mass

These properties are currently attached to a SPRITE struct since every sprite added constitutes a new object.

```
#!c++
SPRITE new_object;
new_object.position.x = rand() % SCREEN_WIDTH;
new_object.position.y = rand() % SCREEN_HEIGHT;
new_object.velocity.x = -10 + rand() % (20 + 1);
new_object.velocity.y =-10 + rand() % (20 + 1);
new_object.width = 201;//in pixels
new_object.height = 201;
new_object.restitution = (float)rand() / (float)RAND_MAX;
new_object.mass = rand() % 50 + 1;
```

Given this information, the engine will move each object and check for collisions with each update. When a collision is registered, the collision will be resolved.

## **Features** ##
Impulse resolution provides an elegant way to simulate 2D physics by narrowing collision handling to the application of an instantaneous change in velocity, or impulse, to the objects involved. This impulse is expressed as a vector with a magnitude and a direction. In short, when a collision is detected, the engine pushes the objects that have collided with a certain force in some direction. A mathematical formula calculates the force of the impulse accounting for each object's velocity, restitution, and mass.

**Restitution**
An object's restitution refers to it's capacity to engage in elastic collisions (how "bouncy" the object is). Restitution is gauged as a value between 0 and 1, with 1 denoting a fully elastic collision. In a fully elastic collision, there is no loss of kinetic energy upon collision, so the objects retain their full velocity [1].

![ElasticCollision.gif](https://bitbucket.org/repo/yRq98d/images/1059695952-ElasticCollision.gif) [1]

If restitution is set to value less than 1, the collision will be partially elastic. This means objects will partially lose velocity after colliding [2].

![InelasticCollision.gif](https://bitbucket.org/repo/yRq98d/images/192306501-InelasticCollision.gif) [2]

A restitution value of 0 refers to a completely inelastic collision (objects don't bounce whatsoever). In a fully inelastic collision, there is a total loss of velocity, so it appears as if the objects have stuck together.

**Mass**
An object's mass will determine how much force it takes to stop or move the object. For a collision where one object is 10 times more massive than the other, we can expect that the more massive object will barely budge upon collision if it's initially at rest [3]. An object with infinite mass can be created by setting its mass property to 0.

![MassCollision.gif](https://bitbucket.org/repo/yRq98d/images/2524519256-MassCollision.gif) [3]

**Collision Normal**
The collision normal is a vector of size 1 storing the direction in which the impulse will be applied for each object. For a collision between two objects, the collision normal is calculated as the vector between the center of mass of each object. This method allows for intuitive object deflections [4].

![CollisionTest.gif](https://bitbucket.org/repo/yRq98d/images/325417226-CollisionTest.gif) [4]