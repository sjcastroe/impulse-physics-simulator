#ifndef GAME_PHYSICS	
#define GAME_PHYSICS

#include "game.h"

class Object
{
public:
	Object();
	Object(int x, int y);
	int get_x() const;
	int get_y() const;
	virtual float get_radius() const = 0;
	virtual int get_left() const = 0;
	virtual int get_right() const = 0;
	virtual int get_top() const = 0;
	virtual int get_bottom() const = 0;

private:
	int x_center, y_center;
};

class Circle : public Object
{
public:
	Circle();
	Circle(int x, int y, float rad);
	float get_radius() const;
	int get_left() const;
	int get_right() const;
	int get_top() const;
	int get_bottom() const;
private:
	float radius;
};

//
class AABB : public Object
{
public:
	AABB();
	AABB(int l, int r, int t, int b, int x, int y);
	float get_radius() const;
	int get_left() const;
	int get_right() const;
	int get_top() const;
	int get_bottom() const;
private:
	int left, right, top, bottom;
};


//Sprite structure
typedef struct {
	D3DXVECTOR2 position;
	D3DXVECTOR2 velocity;
	float restitution, mass, inv_mass;
	int width, height;
	int curframe, lastframe;
	int animdelay, animcount;
} SPRITE;

void SetInvMass(SPRITE &sprite);

//Manifold stores information pertinent to a collision 
//(which objects have collided, the distance of penetration, the vector of the distance between them)
typedef struct {
	Object* object1;
	Object* object2;
	float penetration;
	D3DXVECTOR2 normal;
} Manifold;

float Clamp(float &input, float min, float max);
bool CirclevsCircle(Manifold* m);
bool AABBvsAABB(Manifold* m);
bool AABBvsCircle(Manifold* m);
void Resolve_Collision(SPRITE &A, SPRITE &B, Manifold m);
void Positional_Correction(SPRITE &A, SPRITE &B, Manifold m);

#endif