#include "game_physics.h"

Object::Object()
{
	x_center = 0;
	y_center = 0;
}

Object::Object(int x, int y)
{
	x_center = x;
	y_center = y;
}

int Object::get_x() const
{
	return x_center;
}

int Object::get_y() const
{
	return y_center;
}

float Object::get_radius() const
{
	return 0;
}

int Object::get_left() const
{
	return 0;
}

int Object::get_right() const
{
	return 0;
}

int Object::get_top() const
{
	return 0;
}

int Object::get_bottom() const
{
	return 0;
}

Circle::Circle() : Object(0, 0)
{
	radius = 0;
}

Circle::Circle(int x, int y, float rad) : Object(x, y)
{
	radius = rad;
}

float Circle::get_radius() const
{
	return radius;
}

int Circle::get_left() const
{
	return 0;
}

int Circle::get_right() const
{
	return 0;
}

int Circle::get_top() const
{
	return 0;
}

int Circle::get_bottom() const
{
	return 0;
}

AABB::AABB() : Object(0, 0)
{
	left = 0;
	right = 0;
	top = 0;
	bottom = 0;
}

AABB::AABB(int l, int r,int t, int b, int x, int y) : Object(x, y)
{
	left = l;
	right = r;
	top = t;
	bottom = b;
}

float AABB::get_radius() const
{
	return 0;
}

int AABB::get_left() const
{
	return left;
}

int AABB::get_right() const
{
	return right;
}

int AABB::get_top() const
{
	return top;
}

int AABB::get_bottom() const
{
	return bottom;
}

void SetInvMass(SPRITE &sprite)
{
	if(sprite.mass == 0)
		sprite.inv_mass = 0;
	else
		sprite.inv_mass = 1 / sprite.mass;
}

float Clamp(float &input, float min, float max)
{
	if(input > max)
	{
		input = max;
		return input;
	}
	if(input < min)
	{
		input = min;
		return input;
	}
	else
		return input;
}


bool CirclevsCircle(Manifold* m)
{
	Object* A = m -> object1;
	Object* B = m -> object2;

	D3DXVECTOR2 a_pos((float)A -> get_x(), (float)A -> get_y());
	D3DXVECTOR2 b_pos((float)B -> get_x(), (float)B -> get_y());
	D3DXVECTOR2 n = b_pos - a_pos;

	float radius = A -> get_radius() + B -> get_radius();

	if( n.x * n.x + n.y * n.y > radius * radius)
		return false;

	float n_length = sqrt(n.x * n.x + n.y * n.y);

	if (n_length != 0)
	{
		m -> penetration = radius - n_length;
		m -> normal = n / n_length;
		return true;
	}
	else
	{
		m -> penetration = A -> get_radius();
		m -> normal = D3DXVECTOR2(1, 0);
		return true;
	}
}

bool AABBvsAABB(Manifold* m)
{
	Object* A = m -> object1;
	Object* B = m -> object2;

	D3DXVECTOR2 a_pos((float)A -> get_x(), (float)A -> get_y());
	D3DXVECTOR2 b_pos((float)B -> get_x(), (float)B -> get_y());
	D3DXVECTOR2 n = b_pos - a_pos;

	float a_extentX = (float) (A -> get_right() - A -> get_left()) / 2;
	float b_extentX = (float) (B -> get_right() - B -> get_left()) / 2;

	float x_overlap = a_extentX + b_extentX - abs(n.x);

	if(x_overlap > 0)
	{
		float a_extentY = (float) (A -> get_bottom() - A -> get_top()) / 2;
		float b_extentY = (float) (B -> get_bottom() - B -> get_top()) / 2;

		float y_overlap = a_extentY + b_extentY - abs(n.y);

		if(y_overlap > 0)
		{
			if(x_overlap > y_overlap)
			{
				if(n.x < 0)
					m -> normal = D3DXVECTOR2(0, -1);
				else
					m -> normal = D3DXVECTOR2(0, 1);
				m -> penetration = x_overlap;
				return true;
			}
			else
			{
				if(n.y < 0)
					m -> normal = D3DXVECTOR2(-1, 0);
				else
					 m -> normal = D3DXVECTOR2(1, 0);
				m -> penetration = y_overlap;
				return true;
			}
		}
		return false;
	}
	return false;
}

bool AABBvsCircle(Manifold* m)
{
	Object* A = m -> object1;
	Object* B = m -> object2;

	D3DXVECTOR2 a_pos((float) A -> get_x(), (float) A -> get_y());
	D3DXVECTOR2 b_pos((float) B -> get_x(), (float) B -> get_y());
	D3DXVECTOR2 n = b_pos - a_pos;

	D3DXVECTOR2 closest = n;

	float x_extent = (float) (A -> get_right() - A -> get_left()) / 2;
	float y_extent = (float) (A -> get_bottom() - A -> get_top()) / 2;

	closest.x = Clamp(closest.x, -x_extent, x_extent);
	closest.y = Clamp(closest.y, -y_extent, y_extent);

	bool inside = false;

	if(n == closest)
	{
		inside = true;

		if(abs(n.x) > abs(n.y))
		{
			if(closest.x > 0)
				closest.x = x_extent;
			else
				closest.x = -x_extent;
		}

		else
		{
			if(closest.y > 0)
				closest.y = y_extent;
			else
				closest.y = -y_extent;
		}
	}

	D3DXVECTOR2 normal = n - closest;
	float normal_length = normal.x * normal.x + normal.y * normal.y;
	float rad = B -> get_radius();

	if(normal_length > rad * rad && !inside)
		return false;
	
	normal_length = sqrt(normal_length);

	if(inside)
	{
		m -> normal = -normal / normal_length;
		m -> penetration = rad - normal_length;
	}
	else
	{
		m -> normal = normal / normal_length;
		m -> penetration = rad - normal_length;
	}
	return true;
}
	
void Resolve_Collision(SPRITE &A, SPRITE &B, Manifold m)
{
	D3DXVECTOR2 rv = B.velocity - A.velocity;

	float velAlongNormal = rv.x * m.normal.x + rv.y * m.normal.y;

	if (velAlongNormal > 0)
		return;

	float restitution = min(A.restitution, B.restitution);

	float j = -(1 + restitution) * velAlongNormal;
	j /= A.inv_mass + B.inv_mass;

	D3DXVECTOR2 impulse = j * m.normal;
	A.velocity -= A.inv_mass * impulse;
	B.velocity += B.inv_mass * impulse;
}

void Positional_Correction(SPRITE &A, SPRITE &B, Manifold m)
{
	const float percent = 0.2f;//Usually 20% to 80%
	const float slop = 0.01f;//Usually 0.01 to 0.1

	D3DXVECTOR2 correction = max(m.penetration - slop, 0.0f) / (A.inv_mass + B.inv_mass) * percent * m.normal;

	A.position -= A.inv_mass * correction;
	B.position += B.inv_mass * correction;
}