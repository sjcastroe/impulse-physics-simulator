#ifndef GAME_H
#define GAME_H

#include <d3d9.h>
#include <d3dx9.h>
#include <d3dx9math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "dxgraphics.h"

//application title
#define APPTITLE L"Collisions Test"

//screen setup
#define FULLSCREEN 0
#define SCREEN_WIDTH 1366//640
#define SCREEN_HEIGHT 768//480

//macros to read the keyboard asynchronously
#define KEY_DOWN(vk_code)((GetAsyncKeyState(vk_code)&0x8000)?1:0)
#define KEY_UP(vk_code)((GetAsyncKeyState(vk_code)&0x8000)?1:0)

int Game_Init(HWND);
void Game_Run(HWND);
void Game_End(HWND);

#endif