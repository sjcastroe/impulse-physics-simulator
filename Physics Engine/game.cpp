//Collision Test Program

#include "game.h"
#include "game_physics.h"

const float fps = 60;//Framerate
const float dt = (1 / fps) * 1000;//Standard time interval (in milliseconds)
float accumulator = 0;
float frameStart = GetTickCount();

//number of balls on the screen
#define NUMBALLS 5

LPDIRECT3DTEXTURE9 ball_image;
SPRITE balls[NUMBALLS];
LPDIRECT3DSURFACE9 back;
LPD3DXSPRITE sprite_handler;
HRESULT result;

//timing variable
long start = GetTickCount();

void MoveBalls(HWND hwnd)
{
	int n, m;
	int collidee = -1;

	//keeps the balls in motion
	for (n = 0; n < NUMBALLS; n++)
	{
		balls[n].position += balls[n].velocity;

		//bounce the ball at screen edges
		if(balls[n].position.x > SCREEN_WIDTH - balls[n].width)//if ball hits right wall
		{
			balls[n].position.x -= 1;
			balls[n].velocity.x *= -1;
		}

		else if (balls[n].position.x < 0)//if ball hits left wall
		{
			balls[n].position.x += 1;
			balls[n].velocity.x *= -1;
		}

		if(balls[n].position.y > SCREEN_HEIGHT - balls[n].height)//if ball hits bottom wall
		{
			balls[n].position.y -= 1;
			balls[n].velocity.y *= -1;
		}

		else if (balls[n].position.y < 0)//if ball hits top wall
		{
			balls[n].position.y += 1;
			balls[n].velocity.y *= -1;
		}

		//Checks that the ball has not already been collided with
		if(n != collidee)
		{
			//check for collision with other balls
			for (m = 0; m < NUMBALLS; m++)
			{
				//ignore current ball (can't collide with itself)
				if (n == m) continue;//ignore current ball
				Manifold collision;
				Object* obj_1 = new Circle((int)balls[n].position.x + balls[n].width / 2, (int)balls[n].position.y + balls[n].height / 2,
												(float)balls[n].height / 2);
				Object* obj_2 = new Circle((int)balls[m].position.x + balls[m].width/2, (int)balls[m].position.y + balls[m].height/2,
											(float)balls[m].height/2);

				collision.object1 = obj_1;
				collision.object2 = obj_2;
				if (CirclevsCircle(&collision))//if balls collided
				{
					collidee = m;
					Resolve_Collision(balls[n], balls[m], collision);
					Positional_Correction(balls[n], balls[m], collision);
				}//collision

				delete obj_1;
				delete obj_2;
			}//collision with other balls
		}//Checks that ball has not already been collided with
	}//keeps the balls in motion
}

void DrawBalls()
{
	int n;
	D3DXVECTOR3 position(0, 0, 0);//ball position vector

	//draw the balls
	for (n = 0; n < NUMBALLS; n++)
	{
		position.x = (float)balls[n].position.x;
		position.y = (float)balls[n].position.y;
		sprite_handler -> Draw(
			ball_image,
			NULL,
			NULL,
			&position,
			D3DCOLOR_XRGB(255, 255, 255));
	}
}

//initializes the game
int Game_Init(HWND hwnd)
{
	//set random number seed
	srand( (unsigned int) time(NULL));

	//create sprite handler object
	result = D3DXCreateSprite(d3ddev, &sprite_handler);
	if (result != D3D_OK)
		return 0;

	//load the background image
	back = LoadSurface(L"..\\Physics Engine\\sprites\\background.png", NULL);
	if (back == NULL)
		return 0;

	//load the ball sprite
	ball_image = LoadTexture(L"..\\Physics Engine\\sprites\\redball.png", D3DCOLOR_XRGB(255, 0, 255));
	if (ball_image == NULL)
		return 0;

	//set the balls' properties
	for (int n = 0; n < NUMBALLS; n++)
	{
		balls[n].position.x = rand() % (SCREEN_WIDTH - 201 + 1);
		balls[n].position.y = rand() % (SCREEN_HEIGHT - 201 + 1);
		balls[n].velocity.x = -10 + rand() % (20 + 1);
		balls[n].velocity.y = -10 + rand() % (20 + 1);
		balls[n].width = 201;
		balls[n].height = 201;
		/*restitution = 1 object will collide elastically(no loss in kinetic energy)
		retiturion < 1 object will collide inelastically (some loss in kinetic energy)*/
		balls[n].restitution = (float)rand() / (float)RAND_MAX;
		balls[n].mass = rand() % 50 + 1;
		SetInvMass(balls[n]);
	}
	/*
		balls[1].position.x = 800;//rand() % SCREEN_WIDTH;
		balls[1].position.y = 300;//rand() % SCREEN_HEIGHT;
		balls[1].velocity.x = 0;//rand()%11 - 5;
		balls[1].velocity.y = 0;//rand()%11 - 5;
		balls[1].width = 201;
		balls[1].height = 201;
		balls[1].restitution = 0.5;//(float)rand() / (float)RAND_MAX;
		balls[1].mass = 10;//rand() % 50 + 1;
		SetInvMass(balls[1]);
	*/
	return 1;
}

//the main game loop
void Game_Run(HWND hwnd)
{
	//make sure the Direct3D device is valid
	if(d3ddev == NULL)
		return;

	const float currentTime = GetTickCount();

	accumulator += currentTime - frameStart;

	frameStart = currentTime;

	//avoid spiral of death by clamp how many times Physics_Update runs
	//(ex. if dt is 0.016 (16 milliseconds) the max number of times Physics_Update can run is 12
	if(accumulator > 200.0f)
		accumulator = 200.0f;

	while(accumulator > dt)
	{
		MoveBalls(hwnd);
		accumulator -= dt;
	}

	const float alpha = accumulator / dt;//UNUSED FOR NOW

	//start rendering 
	d3ddev -> BeginScene();
	
	//erase the entire background
	d3ddev -> StretchRect(back, NULL, backbuffer, NULL, D3DTEXF_NONE);

	//start sprite handler
	sprite_handler -> Begin(D3DXSPRITE_ALPHABLEND);

	//draw the sprites
	DrawBalls();

	//stop drawing
	sprite_handler->End();

	//stop rendering
	d3ddev->EndScene();

	//display the back buffer on the screen
	d3ddev-> Present(NULL, NULL, NULL, NULL);
	
	//check for escape key to exit program
	if (KEY_DOWN(VK_ESCAPE))
		PostMessage(hwnd, WM_DESTROY, 0, 0);
}

//frees memory and cleans up before the game ends
void Game_End(HWND hwnd)
{
	if (ball_image != NULL)
		ball_image->Release();

	if(back != NULL)
		back->Release();

	if(sprite_handler != NULL)
		sprite_handler -> Release();
}